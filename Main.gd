extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export (PackedScene) var Mob
var score

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	randomize()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func game_over():
	$ScoreTimer.stop()
	$MobTimer.stop()
	
	for mob in get_children():
		if mob.has_method("_on_VisibilityNotifier2D_screen_exited"):
			mob.hide()
	
	$HUD.show_game_over()
	
	$Music.stop()
	$DeathSound.play()


func new_game():
	score = 0
	$Player.start($StartPosition.position)
	$StartTimer.start()
	
	$HUD.update_score(score)
	$HUD.show_message("Get Ready!")
	
	$Music.play()


func _on_StartTimer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()


func _on_ScoreTimer_timeout():
	score += 1
	
	$HUD.update_score(score)


func _on_MobTimer_timeout():
	# Chose a random location on Path2D
	$MobPath/MobSpawnLocation.set_offset(randi())
	# Create a Mob instance and add it to the scene
	var mob = Mob.instance()
	add_child(mob)
	
	# Set the mob direction perpendicluar to the path direction
	var direction = $MobPath/MobSpawnLocation.rotation + PI / 2
	
	# Set the mobs spawn location to a random location
	mob.position = $MobPath/MobSpawnLocation.position
	
	# Add some randomness to the direction
	direction += rand_range(-PI / 4, PI / 4)
	mob.rotation = direction
	# Chose the velocity
	mob.set_linear_velocity(Vector2(rand_range(mob.min_speed, mob.max_speed), 0).rotated(direction))
	
	
	


func _on_HUD_start_game():
	new_game()


func _on_Player_hit():
	game_over()
